/* Calculate mean square displacement from LAMMPS text dump */
/* NB! There's not much error checking being done here! */
/* NB! Compiling the code will probably result in lots of warnings about unchecked return value of fgets and fscanf. Checkes are omitted for performance reasons (although no profiling has been performed to prove it is useful */
/* Original author: Stefan Parviainen <stefan.parviainen@iki.fi>, May 2015 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define LINELEN 1024
#define MAXTYPES 256
#define MAXCOLS 256
#define MAXCOLLEN 256

struct atom {
	double coords[3];
	int include;
};
typedef struct atom atom_t;

void print_usage(char* name) {
	printf("Usage: %s [options] infile1 [infile2] [infile3] ...\n", name);
	printf("Options:\n");
	printf("\t--cm\tCompensate for drift in center of mass\n");
	printf("\t--types type1[,type2,..]\tOnly include atoms of given types (numeric, comma separated)\n");
	printf("\t--minx VALUE\tOnly include atoms with x < VALUE\n");
	printf("\t--miny VALUE\tOnly include atoms with y < VALUE\n");
	printf("\t--minz VALUE\tOnly include atoms with z < VALUE\n");
	printf("\t--maxx VALUE\tOnly include atoms with x > VALUE\n");
	printf("\t--maxy VALUE\tOnly include atoms with y > VALUE\n");
	printf("\t--maxz VALUE\tOnly include atoms with z > VALUE\n");
}

int parse_atom_data(char* line, char columns[MAXCOLS][MAXCOLLEN], double* coords, int* atomid, int* atomtype) {
	char* l = line;
	char* s = strtok(l, " ");
	int i;
	*atomid = -1;
	for(i = 0; s != NULL && i < MAXCOLS; i++) {
		if(strncmp(columns[i], "id", MAXCOLLEN) == 0) *atomid = atoi(s);
		else if(strncmp(columns[i], "x", MAXCOLLEN) == 0) coords[0] = atof(s);
		else if(strncmp(columns[i], "y", MAXCOLLEN) == 0) coords[1] = atof(s);
		else if(strncmp(columns[i], "z", MAXCOLLEN) == 0) coords[2] = atof(s);
		else if(strncmp(columns[i], "type", MAXCOLLEN) == 0) *atomtype = atoi(s);
		s = strtok(NULL," ");
	}
	if(*atomid == -1) {
		return 1;
	}

	*atomid -= 1; /* Array indices start from 0, while LAMMPS indexes start from 0 */
	return 0;
}
int main(int argc, char**argv) {
	FILE* file;
	char line[LINELEN];

	int time;
	int pb[3];
	int atomcount;
	double bounds[3][2];
	double width[3];
	double maxc[3],minc[3];
	int compensate_cm = 0;

	int atomid,atomtype;
	double coords[3];

	atom_t* original;

	double original_cm[3];
	double cm[3],delta_cm[3];

	int first;

	double msd[4];
	int types[MAXTYPES];
	char columns[MAXCOLS][MAXCOLLEN];

	int i;

	for(i=0;i<3;i++) {
		pb[i] = 0;
		width[i] = 0;
	}

	time = -1;
	first = 1;

	atomcount = -1;
	original = NULL;

	if(argc < 2) {
		print_usage(argv[0]);
		return 1;
	}

	types[0] = -1;

	/* BUG: Coordinates in input can't be bigger than this */
	maxc[0] = maxc[1] = maxc[2] = 1e99;
	minc[0] = minc[1] = minc[2] = -1e99;

	/* Loop over each command line argument */
	int iarg;
	for(iarg = 1; iarg < argc; iarg++) {
		if(strcmp(argv[iarg],"--help") == 0) {
			print_usage(argv[0]);
			return 0;
		}
		if(strcmp(argv[iarg],"--cm") == 0) {
			compensate_cm = 1;
			iarg++;
		}
		if(strcmp(argv[iarg],"--types") == 0) {
			iarg++;
			char* s;
			int i=0;
			s = strtok(argv[iarg],",");
			while(s != NULL) {
				types[i++] = atoi(s);
				s = strtok(NULL, ",");
			}
			types[i] = -1;
			iarg++;	
		}
		if(strcmp(argv[iarg],"--minx") == 0) {
			iarg++;
			minc[0] = atof(argv[iarg]);
			iarg++;
		}
		if(strcmp(argv[iarg],"--maxx") == 0) {
			iarg++;
			maxc[0] = atof(argv[iarg]);
			iarg++;
		}
		if(strcmp(argv[iarg],"--miny") == 0) {
			iarg++;
			minc[1] = atof(argv[iarg]);
			iarg++;
		}
		if(strcmp(argv[iarg],"--maxy") == 0) {
			iarg++;
			maxc[1] = atof(argv[iarg]);
			iarg++;
		}
		if(strcmp(argv[iarg],"--minz") == 0) {
			iarg++;
			minc[2] = atof(argv[iarg]);
			iarg++;
		}
		if(strcmp(argv[iarg],"--maxz") == 0) {
			iarg++;
			maxc[2] = atof(argv[iarg]);
			iarg++;
		}

		/* Some error checking */
		else if(strncmp(argv[iarg],"--",2) == 0) {
			printf("Unknown option %s\n",argv[iarg]);
			return 1;
		}

		file = fopen(argv[iarg],"r");
		if(file == NULL) {
			printf("No such file: %s\n",argv[iarg]);
			return 1;
		}

		/* Keep reading lines from input file */
		while(1) {
			fgets(line, LINELEN, file);
			if(feof(file)) break; /* Stop if end of file has been reached */
			
			/* Handle section with atom positions */
			if(strstr(line,"ITEM: ATOMS") != NULL) {
				char* l = line;
				l += strlen("ITEMS: ATOMS");
				char* s = strtok(l," ");

				int i = 0;
				/* Read in columns */
				while(s != NULL) {
					strncpy(columns[i++], s, MAXCOLLEN);
					s = strtok(NULL," ");
				}

				for(i=0;i<4;i++) msd[i] = 0;

				/* Loop over atoms, one line at a time */
				int iat;
				int counted = 0;

				if(compensate_cm) {
					long int filepos = ftell(file);
					for(i=0;i<3;i++) cm[i] = 0;
					for(iat=0; iat < atomcount; iat++) {
						fgets(line, LINELEN, file);
						if(parse_atom_data(line,columns,coords,&atomid,&atomtype) > 0) {
							return 1;
						}
						int i;
						for(i=0;i<3;i++) cm[i] += coords[i];
					}
					for(i=0;i<3;i++) cm[i] /= atomcount;
					if(first) {for(i=0;i<3;i++) original_cm[i] = cm[i]; }
					for(i=0;i<3;i++) delta_cm[i] = cm[i] - original_cm[i];

					fseek(file,filepos,SEEK_SET);
				}
				for(iat=0; iat < atomcount; iat++) {

					/* Read line containing atom info */
					fgets(line, LINELEN, file);

					if(parse_atom_data(line,columns,coords,&atomid,&atomtype) > 0) {
						return 1;
					}

					if(compensate_cm) {
						int i;
						for(i=0;i<3;i++) coords[i] -= delta_cm[i];
					}

					/* Remember the original position of an atom */
					if(first && atomid != -1) {
						int i;
						for(i=0; i < 3; i++) {
						       	original[atomid].coords[i] = coords[i];
						}
						/* Check if atom type is in the list of wanted types */
						original[atomid].include = (types[0] == -1); /* Maybe all types are wanted? */
						for(i=0; types[i] != -1 && i < MAXTYPES; i++) {
							if(atomtype == types[i]) { original[atomid].include = 1; break; }
						}
					}

					if(atomid == -1) {
						fprintf(stderr, "It seems atom IDs were not stored in the input file.\nExiting.");
						return 1;
					}

					if(original[atomid].include && (coords[0] > minc[0] && coords[0] < maxc[0]) && (coords[1] > minc[1] && coords[1] < maxc[1]) && (coords[2] > minc[2] && coords[2] < maxc[2])) {
						/* Calculate square displacement, accounting for periodic boundaries */
						for(i=0;i<3;i++) {
							double delta = original[atomid].coords[i] - coords[i];
							if(pb[i] && abs(delta) > width[i]/2) delta = width[i] - abs(delta);
							msd[i] += pow(delta, 2);
						}
						counted++;
					}
				}
				msd[3] = msd[0] + msd[1] + msd[2];

				/* Calculate mean square displacement */
				for(i=0;i<4;i++) msd[i] = msd[i] / counted;

				printf("%d %f %f %f %f\n", time, msd[0],msd[1],msd[2],msd[3]);
				first = 0;
			}

			/* Handle section containing the current timestep */
			else if(strstr(line,"ITEM: TIMESTEP") != NULL) {
				fscanf(file,"%d\n",&time);
			}
			/* Handle section containing box size and boundary conditions */
			/* BUG: Only handles fully periodic boundaries and open boundaries */
			else if(strstr(line,"ITEM: BOX BOUNDS") != NULL) {
				char pbx[8],pby[8],pbz[8];
				sscanf(line,"ITEM: BOX BOUNDS %s %s %s",pbx,pby,pbz);
				pb[0] = (strncmp(pbx,"pp",2) == 0);
				pb[1] = (strncmp(pby,"pp",2) == 0);
				pb[2] = (strncmp(pbz,"pp",2) == 0);

				fscanf(file,"%lf %lf\n%lf %lf\n%lf %lf",&bounds[0][0],&bounds[0][1],&bounds[1][0],&bounds[1][1],&bounds[2][0],&bounds[2][1]);
				width[0] = bounds[0][1]-bounds[0][0];
				width[1] = bounds[1][1]-bounds[1][0];
				width[2] = bounds[2][1]-bounds[2][0];
			}
			/* Handle section containing the number of atoms */
			/* BUG: Number of atoms in not allowed to change */
			else if(strstr(line,"ITEM: NUMBER OF ATOMS") != NULL) {
				fscanf(file,"%d\n",&atomcount);
				if(first) {
					original = malloc(atomcount*sizeof(atom_t));
				}
			}
		}
		fclose(file);
	}
	return 0;
}
