/* fixlammpsids.c */
/* Correct the ID of atoms in a lammps dump file based on the correct IDs in an earlier dump file */
/* Works by finding the ID of the closest atom in the previous dump file, so time between the frames shouldn't be too large */
/* Original author Stefan Parviainen <stefan.parviainen@iki.fi>, May 2015 */

/* Compile with e.g. cc fixlammpsids.c -lm -O3 -o fixlammpsids */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define LINELEN 1024
#define MAXTYPES 256
#define MAXCOLS 256
#define MAXCOLLEN 256

struct atom {
	double coords[3];
};
typedef struct atom atom_t;

int parse_atom_data(char* line, char columns[MAXCOLS][MAXCOLLEN], double* coords, int* atomid, int* atomtype) {
	char* l;
	/* Make a copy of line so it's not destroyed by strtok */
	l = malloc((strlen(line)+1)*sizeof(char));
	strcpy(l,line);

	char* s = strtok(l, " ");
	int i;
	*atomid = -1;
	for(i = 0; s != NULL && i < MAXCOLS; i++) {
		if(strncmp(columns[i], "id", MAXCOLLEN) == 0) *atomid = atoi(s);
		else if(strncmp(columns[i], "x", MAXCOLLEN) == 0) coords[0] = atof(s);
		else if(strncmp(columns[i], "y", MAXCOLLEN) == 0) coords[1] = atof(s);
		else if(strncmp(columns[i], "z", MAXCOLLEN) == 0) coords[2] = atof(s);
		else if(strncmp(columns[i], "type", MAXCOLLEN) == 0) *atomtype = atoi(s);
		s = strtok(NULL," ");
	}
	if(*atomid == -1) {
		return 1;
	}

	free(l);

	*atomid -= 1; /* Array indices start from 0, while LAMMPS indexes start from 0 */
	return 0;
}

int main(int argc, char** argv) {
	FILE* file;
	char line[LINELEN];

	int iarg;

	int firstfile = 1;
	int firstframe = 1;
	int* idmap;
	char columns[MAXCOLS][MAXCOLLEN];
	int atomcount;

	int atomid,atomtype;
	double coords[3];

	atom_t* prev;
	int* used;
	int pb[3];
	double bounds[3][2];
	double width[3];

	for(iarg = 1; iarg < argc; iarg++) {

		file = fopen(argv[iarg],"r");
		if(file == NULL) {
			printf("No such file: %s\n",argv[iarg]);
			return 1;
		}

		firstframe = 1;
		if(!firstfile) memset(used,0,sizeof(int)*atomcount);

		/* Keep reading lines from input file */
		while(1) {
			fgets(line, LINELEN, file);
			if(feof(file)) break; /* Stop if end of file has been reached */
			printf("%s",line);
			
			/* Handle section with atom positions */
			if(strstr(line,"ITEM: ATOMS") != NULL) {
				char* l = line;
				l += strlen("ITEMS: ATOMS");
				char* s = strtok(l," ");

				int i = 0;
				/* Read in columns */
				while(s != NULL) {
					strncpy(columns[i++], s, MAXCOLLEN);
					s = strtok(NULL," ");
				}

				/* Loop over atoms, one line at a time */
				int iat;
				for(iat=0; iat < atomcount; iat++) {
					/* Read line containing atom info */
					fgets(line, LINELEN, file);

					if(parse_atom_data(line,columns,coords,&atomid,&atomtype) > 0) {
						return 1;
					}

					if(atomid < 0) atomid = iat;

					if(firstfile) {
						idmap[atomid] = atomid;
						if(firstframe) for(i=0;i<3;i++) prev[idmap[atomid]].coords[i] = coords[i];
					}

					double rmin=1e99;
					if(!firstfile && firstframe) {
						int iat2;
						for(iat2 = 0; iat2 < atomcount; iat2++) {
							if(used[iat2]) continue; /* Skip already used atom ids */
							double r = 0;
							int i;
							for(i=0;i<3;i++) {
								double delta = prev[iat2].coords[i] - coords[i];
								if(pb[i] && abs(delta) > width[i]/2) delta = width[i] - abs(delta);
								r += pow(delta,2);
							}

							if(r < rmin) {
								idmap[atomid] = iat2;
								rmin = r;
							}
						}
						used[idmap[atomid]] = 1;
					}
					char* s = strtok(line, " ");
					for(i = 0; s != NULL && i < MAXCOLS; i++) {
						if(strncmp(columns[i], "id", MAXCOLLEN) == 0) printf("%d ",idmap[atomid]+1);
						else if(s[strlen(s)-1] == '\n')  printf("%s",s);
						else printf("%s ",s);
						s = strtok(NULL," ");
					}

					for(i=0;i<3;i++) prev[idmap[atomid]].coords[i] = coords[i];


				}
				firstframe = 0;
			}
			/* Handle section containing box size and boundary conditions */
			/* BUG: Only handles fully periodic boundaries and open boundaries */
			else if(strstr(line,"ITEM: BOX BOUNDS") != NULL) {
				char pbx[8],pby[8],pbz[8];
				sscanf(line,"ITEM: BOX BOUNDS %s %s %s",pbx,pby,pbz);
				pb[0] = (strncmp(pbx,"pp",2) == 0);
				pb[1] = (strncmp(pby,"pp",2) == 0);
				pb[2] = (strncmp(pbz,"pp",2) == 0);

				fscanf(file,"%lf %lf\n%lf %lf\n%lf %lf",&bounds[0][0],&bounds[0][1],&bounds[1][0],&bounds[1][1],&bounds[2][0],&bounds[2][1]);
				printf("%f %f\n%f %f\n%f %f",bounds[0][0],bounds[0][1],bounds[1][0],bounds[1][1],bounds[2][0],bounds[2][1]);
				width[0] = bounds[0][1]-bounds[0][0];
				width[1] = bounds[1][1]-bounds[1][0];
				width[2] = bounds[2][1]-bounds[2][0];
			}
			/* Handle section containing the number of atoms */
			/* BUG: Number of atoms in not allowed to change */
			else if(strstr(line,"ITEM: NUMBER OF ATOMS") != NULL) {
				fscanf(file,"%d\n",&atomcount);
				if(firstfile && firstframe) {
					used = malloc(atomcount*sizeof(int));
					idmap = malloc(atomcount*sizeof(int));
					prev = malloc(atomcount*sizeof(atom_t));
				}
				printf("%d\n",atomcount);
			}
		}
		firstfile = 0;
		fclose(file);
	}
	return 0;
}
